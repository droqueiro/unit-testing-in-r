#!/usr/bin/env bash

# Invoke the unit tests for the function popmean()
# Note: the warnings are explicity created by the function for different tests cases
Rscript -e "testthat::test_dir('tests')"
