# Scripts and data used to test the function `popmean()`

The repository contains:
- Sub-directory `tests`: 
    - File `test_fn_popmean.R` with the R script to perform the tests
    - File `test_dataframe.tsv` is the data frame used for the tests. Saved as .tsv (tab-separated)
- File `fn_popmean.R` with the source code of the function `popmean()`
- Shell script to execute the tests.

