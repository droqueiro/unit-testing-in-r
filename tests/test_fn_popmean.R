library(testthat)

source("../fn_popmean.R", chdir=FALSE)

# -----------------------------------------------------------------------------
# Unit testing for function popmean()
#
# There are two sets of tests
# Case 1. Using data frame containing a subset of the data in dat2 (file R_exercises_candidate.R)
#    In this case, the data is balanced:
#    - all interventions have the same number of data points
#    - all interventions have data for the same years
#    - the number of data points for a given intervention is equally divided in
#      the two zones
#
# Case 2. Using a data frame hard-coded in this script
#    In this case, the data is unbalanced:
#    - only one intervention ("none") has a data point for year 2022
#    This case allows to test if: 
#    intervention exists and year exists but intervention AND year does NOT exist
# -----------------------------------------------------------------------------

context("Filters")

# Case 1. Using data frame containing a subset of the data in dat2 (file R_exercises_candidate.R)
df_test <- read.table("test_dataframe.tsv", header=TRUE, sep="\t")
# 'data.frame':	144 obs. of  5 variables:
# $ zone        : Factor w/ 2 levels "Zone 1","Zone 2"
# $ population  : int  For "Zone 1" = 692089, "Zone 2" = 324469
# $ year        : int  Years = 2020, 2021, 2022
# $ intervention: Factor w/ 4 levels "none","IRS","ITN", "IRS+ITN"
# $ PR          : num  values on which to compute the weighted mean

test_that("Intervention exists; Range of years exists", {
    expect_equal(round(popmean(df_test, intervention="none", years=2020:2022), digits=3), 0.114)
    expect_equal(round(popmean(df_test, intervention="IRS", years=2020:2022), digits=3), 0.067)
    expect_equal(round(popmean(df_test, intervention="ITN", years=2020:2022), digits=3), 0.071)
    expect_equal(round(popmean(df_test, intervention="IRS+ITN", years=2020:2022), digits=3), 0.05)
})
#> Test passed

test_that("Intervention exists; Range of years exists, ONLY one value", {
    expect_equal(round(popmean(df_test, intervention="none", years=2020), digits=3), 0.077)
    expect_equal(round(popmean(df_test, intervention="IRS", years=2020), digits=3), 0.077)
    expect_equal(round(popmean(df_test, intervention="ITN", years=2020), digits=3), 0.06)
    expect_equal(round(popmean(df_test, intervention="IRS+ITN", years=2020), digits=3), 0.06)
})
#> Test passed

test_that("Intervention exists; Range of years exists ONLY subset, ", {
    expect_equal(round(popmean(df_test, intervention="none", years=2020:2050), digits=3), 0.114)
})
#> Test passed

test_that("Intervention does NOT exist; Range of years exist", {
    expect_equal(round(popmean(df_test, intervention="BASEL", years=2020:2022), digits=3), NaN)
})
#> Test passed

test_that("Intervention exists; Range of years does NOT exist", {
    expect_equal(round(popmean(df_test, intervention="IRS", years=1900:1910), digits=3), NaN)
})
#> Test passed

test_that("Intervention does NOT exist; Range of years does NOT exist", {
    expect_equal(round(popmean(df_test, intervention="BASEL", years=1900:1910), digits=3), NaN)
})
#> Test passed

# Case 2. Using a data frame hard-coded in this script
# Test data frame
# Note: There is only 1 record for year 2022
df_test = read.delim(text = "zone	population	year	intervention	PR
                     Zone 1	692089	2020	none	0.09
                     Zone 1	692089	2021	none	0.13
                     Zone 1	692089	2022	none	0.18
                     Zone 2	324469	2020	none	0.05
                     Zone 2	324469	2021	none	0.07
                     Zone 1	692089	2020	IRS	0.09
                     Zone 1	692089	2021	IRS	0.08
                     Zone 2	324469	2020	IRS	0.05
                     Zone 2	324469	2021	IRS	0.04", sep = "\t", header = T)

test_that("Intervention exists; Range of years exists; Combination of intervention AND year does NOT exist", {
    expect_equal(round(popmean(df_test, intervention="IRS", years=2022), digits=3), NaN)
    expect_equal(round(popmean(df_test, intervention="none", years=2020:2022), digits=3), 0.116)
})
#> Test passed

test_that("Intervention exists; Range of years exists; One intervention has more data points than the other", {
    expect_equal(round(popmean(df_test, intervention="none", years=2020:2022), digits=3), 0.116)
    expect_equal(round(popmean(df_test, intervention="IRS", years=2020:2021), digits=3), 0.072)
    expect_equal(round(popmean(df_test, intervention="IRS", years=2020:2022), digits=3), 0.072)
})
#> Test passed
